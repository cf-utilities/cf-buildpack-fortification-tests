# cf-buildpack-fortification-tests

![Build Status](https://ci.dachs.dog/api/v1/teams/main/pipelines/f11n/jobs/system-tests/badge)

Used to test [cf-buildpack-fortification](https://bitbucket.org/cf-utilities/cf-buildpack-fortification-tests).

## Updating cf-buildpack-fortification

```shell_session
$ cd cf-buildpack-fortification
$ git pull
$ cd ..
$ git add -p
$ git commit -m "updating submodule"
```

## Pre-requisites

* Install [Ruby](https://www.ruby-lang.org/en/documentation/installation/) v2.1.x
* Install [Bundler](http://bundler.io/) into Ruby at the latest available version
* Install [Yml2Env](https://github.com/EngineerBetter/yml2env/releases) v1.0
* Install [CF CLI](https://github.com/cloudfoundry/cli/releases) at a version that matches your target CF service

Additionally for Windows:

* Install [MinGW](http://www.mingw.org/wiki/Getting_Started)

Then prepare locally

```shell_session
$ git submodule update --init --recursive
$ bundle install
```

## Testing

```shell_session
$ yml2env ci/vars/pcfdev.yml bundle exec rspec
```
