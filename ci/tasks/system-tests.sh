#!/bin/bash

set -e

pushd cf-buildpack-fortification
  make test-prereq cf-login test-system
popd

bundle install
bundle exec rspec