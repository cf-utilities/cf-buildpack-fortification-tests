require 'fileutils'
require 'securerandom'
require 'tmpdir'

describe 'Fortification Buildpack' do
  let(:skip_teardown) { ENV.fetch('SKIP_TEARDOWN', false) }
  let(:cf_api) { ENV.fetch('CF_API') }
  let(:cf_username) { ENV.fetch('CF_USERNAME') }
  let(:cf_password) { ENV.fetch('CF_PASSWORD') }
  let(:app_domain) { ENV.fetch('APP_DOMAIN') }
  let(:org) { ENV.fetch('CF_ORG') }
  let(:space) { ENV.fetch('CF_SPACE') }

  let(:setup_commands) { [] }
  let(:teardown_commands) { [] }

  let(:buildpacks_file_path) { File.join(app_tmp_dir, '.buildpacks') }
  let(:f11n_buildpack_uri) { "file://ignored-component/tmp/staged/app/buildpack-under-test.tgz" }
  let(:ps_buildpack_uri) { "https://bitbucket.org/cf-utilities/cf-buildpack-process-supervisor#master" }
  let(:app_tmp_dir) { Dir.mktmpdir }
  let(:buildpack_url) { 'https://github.com/cloudfoundry/ruby-buildpack.git#master' }
  let(:multi_buildpack_uri) { "https://bitbucket.org/cf-utilities/cf-buildpack-multi" }

  before(:each) do
    @cf_home = Dir.mktmpdir
    ENV['CF_HOME'] = @cf_home
    FileUtils.cp_r("#{fixture_dir}/.", app_tmp_dir)

    login_result = `cf login -a #{cf_api} -u #{cf_username} -p #{cf_password} -o #{org} -s #{space} --skip-ssl-validation`
    expect($?.success?).to be_truthy, "#{login_result}"

    expect_command_to_succeed("tar czf #{app_tmp_dir}/buildpack-under-test.tgz --exclude .git/ --exclude spec/ -C cf-buildpack-fortification .")
    write_buildpacks_file

    push_app

    env_vars.each do |key, value|
      expect_command_to_succeed("cf set-env #{app_name} #{key} #{value}")
    end

    start_app
  end

  after(:each) do
    unless skip_teardown
      `cf delete -f #{app_name}`
      FileUtils.rm_rf @cf_home
      FileUtils.rm_rf buildpacks_file_path
    end
  end

  describe 'log output' do
    let(:run_uuid) { SecureRandom.uuid }
    let(:fixture_dir) { 'spec/system/fixtures/ruby-hello-world' }
    let(:app_name) { "ps-test-rhw-#{run_uuid}" }
    let(:env_vars) {
      {
        'FBP_RP_DISABLE_ORIGIN_PROTECTION' => 'true'
      }
    }

    it 'logs all the things' do
      sleep 1 # HAProxy take a little time to start

      uuid = make_log_request

      output = `cf logs #{app_name} --recent`
      expect($?.success?).to be_truthy

      expect_net_stats_logged(output)
      expect_memory_stats_logged(output)
      expect_proxy_stats_logged(output)
      expect_proxy_requests_logged(output)
      expect_app_stdout_logged(output, uuid)

      sleep 70 # CPU stats are only meaningful after a time, and it wasn't deemed worth making this shorter for testing
      expect_command_to_succeed_and_output("cf logs #{app_name} --recent", '"os":{"stats":{"cpu":{')
    end

    it 'logs OOMs' do
      expect_command_to_succeed("curl -so/dev/null -X POST -d '' #{app_name}.#{app_domain}/oom")
      expect_command_to_succeed_and_output("cf logs #{app_name} --recent", 'out of memory')
    end
  end

  describe 'time outs' do
    let(:fixture_dir) { 'spec/system/fixtures/ruby-slow-app' }
    let(:app_name) { "ps-test-slow-#{SecureRandom.uuid}" }
    let(:env_vars) {
      {
        'FBP_RP_DISABLE_ORIGIN_PROTECTION' => 'true',
        'FBP_RP_DEFAULT_TIMEOUT_SERVER_INACTIVITY' => '1000'
      }
    }

    it 'returns 503s when a request takes too long' do
      delay = 2
      expect_command_to_succeed_and_output("curl -so/dev/null --user foo:bar -w %{http_code} -k https://#{app_name}.#{app_domain}/slow?delay=#{delay}", '503')
    end
  end

  describe 'traffic queueing' do
    let(:fixture_dir) { 'spec/system/fixtures/ruby-slow-app' }
    let(:app_name) { "ps-test-slow-#{SecureRandom.uuid}" }
    let(:env_vars) {
      {
        'FBP_RP_DISABLE_ORIGIN_PROTECTION' => 'true',
        'FBP_RP_APP_MAXCONN' => '1',
        'FBP_RP_DEFAULT_TIMEOUT_QUEUE' => '1'
      }
    }

    it 'returns 503s when a request never reaches the app because inbound requests are throttled' do
      delay = 2

      a = nil
      thread = Thread.new do
        a = `curl -k https://#{app_name}.#{app_domain}/slow?delay=#{delay}`
      end

      b = `curl -k https://#{app_name}.#{app_domain}/slow?delay=#{delay}`
      thread.join

      both = a + b
      expect(both.scan('No server is available to handle this request').length).to eq(1), "Expected exactly one of two requests to return 'No server is available to handle this request'"
      expect(both.scan('503').length).to eq(1), "Expected exactly one of two requests to return 503"
    end
  end

  describe 'toggling logs' do
    let(:fixture_dir) { 'spec/system/fixtures/ruby-hello-world' }
    let(:app_name) { "ps-test-rhw-#{SecureRandom.uuid}" }
    let(:env_vars) {
      {
        'FBP_RP_DISABLE_ORIGIN_PROTECTION' => 'true',
        'PS_DISABLE_SERVICE_F11N_STATS_NETWORK' => 'true',
        'PS_DISABLE_SERVICE_F11N_STATS_MEMORY' => 'true',
        'PS_DISABLE_SERVICE_F11N_STATS_CPU' => 'true',
        'PS_DISABLE_SERVICE_F11N_STATS_HAPROXY' => 'true',
        'PS_DISABLE_SERVICE_F11N_LOGS_HAPROXY' => 'true',
      }
    }

    it 'does not log things that are disabled' do
      sleep 1 # HAProxy take a little time to start

      uuid = make_log_request

      output = `cf logs #{app_name} --recent`
      expect($?.success?).to be_truthy

      expect_net_stats_not_logged(output)
      expect_memory_stats_not_logged(output)
      expect_proxy_stats_not_logged(output)
      expect_proxy_requests_not_logged(output)

      sleep 70 # CPU stats are only meaningful after a time, and it wasn't deemed worth making this shorter for testing
      expect_command_to_succeed_and_not_output("cf logs #{app_name} --recent", '"os":{"stats":{"cpu":{')
    end
  end
end