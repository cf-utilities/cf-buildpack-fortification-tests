def make_log_request
  uuid = SecureRandom.uuid
  expect_command_to_succeed("curl -so/dev/null -X POST -d '#{uuid}' #{app_name}.#{app_domain}/log")
  uuid
end

def expect_logged(text, output)
  expect(output).to include(text)
end

def expect_not_logged(text, output)
  expect(output).to_not include(text)
end

def expect_net_stats_logged(output)
  expect_logged('"os":{"stats":{"network":', output)
end

def expect_net_stats_not_logged(output)
  expect_not_logged('"os":{"stats":{"network":', output)
end

def expect_memory_stats_logged(output)
  expect_logged('"os":{"stats":{"memory":', output)
end

def expect_memory_stats_not_logged(output)
  expect_not_logged('"os":{"stats":{"memory":', output)
end

def expect_proxy_stats_logged(output)
  expect_logged('"proxy":{"stats":{', output)
end

def expect_proxy_stats_not_logged(output)
  expect_not_logged('"proxy":{"stats":{', output)
end

def expect_proxy_requests_logged(output)
  expect_logged('"proxy":{"log":{', output)
  expect_logged('"m":"POST","p":"/log"', output)
end

def expect_proxy_requests_not_logged(output)
  expect_not_logged('"proxy":{"log":{', output)
  expect_not_logged('"m":"POST","p":"/log"', output)
end

def expect_app_stdout_logged(output, uuid)
  expect_logged(uuid, output)
end

def expect_app_stdout_not_logged(output, uuid)
  expect_not_logged(uuid, output)
end